# Robots_icaro

Archivos STL, código fuente y planos para el desarrollo de los dos robots planteados para el proyecto ICARO.

## carpetas

Cada carpeta contiene los archivos de Freecad y los stl para imprimir los siguientes robots:

### robot_dibujador_movil_motores_pap

Este es un robot basado en dos motores pap bj28 (muy comun y barato), y un micro servo de 9g.

Esta diseñado para funcionar con 4 pilas AA y una placa icaro np07 (controlado a traves de la UASART y mediante un HC05). Es el primer modelo de robot dibujador.

### robot_dibujador_movil_motores_pap_V2

Esta versión es mecanimca y electronicamente igual al robot_dibujador_movil_motores_pap, pero el diseño mecanico es mas chico (se tarda menos en imprimir) y en vez de usar una placa icaro, usa el hardware de la carpeta **18f2550_mini_pcb_robots_paps**.

### robot_sumo_10cm

un robot basado en el hardawre icaro np07, con dos motores CC, espacio para un porta pilas de 8 pilas AA, y una pantalla de lcd de 8x2.

### robot_vplotter

Robot diseñado para dibujar sobre una superficie plana, usando un esquema de robot conocidos como V-plotter, dado que tiene forma de V donde el centro esta el cabezal (un lapiz y un micro servo) y en las puntas unas roldanas accionadas por motores paps.

### 18f2550_mini_pcb_robots_paps

Diseño en kicad de un PCB especifico para controlar los robots basados en 2 motores paps y un micro servo de 9g. El hardware esta pensado apra controlar dos motores, un micro servo y enviar y recibir ordenes a traves de el puerto USART (tipicamente un HC05, pero se podria adaptar a un esp826).

No usa regulador de tensión, trabaja directamente con 4 pilas recargables AA (1,2*4 = 4,8V)
